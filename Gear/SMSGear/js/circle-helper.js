/*global tau, Node, DOM*/
/*jslint unparam: true */
(function(tau) {
    var listHelper = [];

    function createSnapListStyle(page) {
        if (tau.support.shape.circle) {
            var list = page.querySelectorAll(".ui-listview");
            if (list) {
                for (i = 0; i < list.length; i++) {
                    if (page.id != "page_conversation_messages") {
                        listHelper[i] = tau.helper.SnapListStyle.create(list[i], {
                            animate: "scale"
                        });
                    }
                }
            }
        }
    }

    function destroySnapListStyle() {
        var len = listHelper.length;
        if (len) {
            for (i = 0; i < len; i++) {
                listHelper[i].destroy();
            }
            listHelper = [];
        }
    }

    document.addEventListener("pagebeforeshow", function(e) {
        // console.log("got a pagebeforeshow event!");
        var page = e.target;
        createSnapListStyle(page);
    });

    document.addEventListener("pagebeforehide", function(e) {
        // console.log("got a pagebeforehide event!");
        destroySnapListStyle();
    });

    document.addEventListener("updatesnaplist", function(e) {
        // console.log("got a onsnaplistitemschanged event!");
				var page = e.target;
        destroySnapListStyle();
        createSnapListStyle(page);
    }, false);
}(tau));

function scrollToBottom(page) {
    var elScroller = page.querySelector(".ui-scroller");
    elScroller.scrollTop = elScroller.scrollHeight;
}

document.addEventListener('rotarydetent', function onRotarydetent(ev) {
    var direction = ev.detail.direction;
    var page = document.querySelector(".ui-page-active");

    if (page && page.id == "page_conversation_messages") {
        var uiScroller = page.querySelector('.ui-scroller');
        var scrollPos = $(uiScroller).scrollTop();

        if (direction === "CW") {
            $(uiScroller).scrollTop(scrollPos + 50);
        } else {
            $(uiScroller).scrollTop(scrollPos - 50);
        }
    }
});
