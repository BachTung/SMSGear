package com.jamesst20.smsgearcompanion;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jamesst20.smsgearcompanion.models.SMSContact;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends Activity {
    private static final int REQUEST_PERMISSION_STARTUP = 100;
    private static final String[] PERMISSIONS = new String[]{Manifest.permission.READ_SMS, Manifest.permission.SEND_SMS, Manifest.permission.READ_CONTACTS};

    private SharedPreferences preferences = null;

    private Spinner spinner_conversationsLimit;
    private Spinner spinner_messagesPerConversation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner_conversationsLimit = (Spinner) findViewById(R.id.spinner_conversationsLimit);
        spinner_messagesPerConversation = (Spinner) findViewById(R.id.spinner_messagesPerConversation);

        (findViewById(R.id.bt_refreshContactsCache)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                refreshContactsCache();
            }
        });

        preferences = getSharedPreferences(getString(R.string.pref_file_key), Context.MODE_PRIVATE);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.conversations_limit_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_conversationsLimit.setAdapter(adapter);
        spinner_conversationsLimit.setOnItemSelectedListener(spinner_on_item_selected);


        adapter = ArrayAdapter.createFromResource(this, R.array.messages_per_conversation_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_messagesPerConversation.setAdapter(adapter);
        spinner_messagesPerConversation.setOnItemSelectedListener(spinner_on_item_selected);

        SharedPreferences.Editor editor = preferences.edit();
        if (!preferences.contains(getString(R.string.pref_conversations_limit))) {
            editor.putInt(getString(R.string.pref_conversations_limit), Integer.parseInt((String) spinner_conversationsLimit.getSelectedItem()));
            editor.putInt(getString(R.string.pref_conversations_limit_index), spinner_conversationsLimit.getSelectedItemPosition());
        }
        if (!preferences.contains(getString(R.string.pref_messages_per_conversation))) {
            editor.putInt(getString(R.string.pref_messages_per_conversation), Integer.parseInt((String) spinner_messagesPerConversation.getSelectedItem()));
            editor.putInt(getString(R.string.pref_messages_per_conversation), spinner_messagesPerConversation.getSelectedItemPosition());
        }
        editor.apply();

        spinner_conversationsLimit.setSelection(preferences.getInt(getString(R.string.pref_conversations_limit_index), 0));
        spinner_messagesPerConversation.setSelection(preferences.getInt(getString(R.string.pref_messages_per_conversation_index), 0));

        if(verifyNeededPermissions()){
            refreshContactsCache();
        }

        System.out.println(Math.abs(new Random().nextInt()));

        System.out.println("{\"orderId\":10, \"packageName\":\"a\", \"productId\":\"dfdsdf\", \"purchaseTime\":1486046763, \"purchaseState\":0, \"developerPayload\":\"ddasds\", \"token\":\"dsfsggffg\", \"purchaseToken\":\"fdssfffgdfg\"}");
    }

    private AdapterView.OnItemSelectedListener spinner_on_item_selected = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            SharedPreferences.Editor editor = preferences.edit();
            switch (parent.getId()) {
                case R.id.spinner_conversationsLimit:
                    editor.putInt(getString(R.string.pref_conversations_limit), Integer.parseInt((String) spinner_conversationsLimit.getItemAtPosition(position)));
                    editor.putInt(getString(R.string.pref_conversations_limit_index), position);
                    break;
                case R.id.spinner_messagesPerConversation:
                    editor.putInt(getString(R.string.pref_messages_per_conversation), Integer.parseInt((String) spinner_messagesPerConversation.getItemAtPosition(position)));
                    editor.putInt(getString(R.string.pref_messages_per_conversation_index), position);
                    break;
            }
            editor.apply();
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private boolean verifyNeededPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean allPermissionsGranted = true;
            for(String permission : PERMISSIONS) {
                if (this.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    allPermissionsGranted = false;
                    break;
                }
            }
            if (!allPermissionsGranted) {
                this.requestPermissions(PERMISSIONS, REQUEST_PERMISSION_STARTUP);
            }
            return allPermissionsGranted;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_STARTUP:
                if (grantResults.length > 0) {
                    boolean allPermissionsGranted = true;
                    for (int result : grantResults) {
                        if (result != PackageManager.PERMISSION_GRANTED) {
                            AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                            alertDialog.setTitle("Error");
                            alertDialog.setMessage("Error. All permissions must be granted, otherwise the application will not work.\nThe application will close.");
                            alertDialog.setCancelable(false);
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                            finish();
                                        }
                                    });
                            alertDialog.show();

                            allPermissionsGranted = false;
                            break;
                        }
                    }
                    if(allPermissionsGranted) {
                        refreshContactsCache();
                    }
                }
        }
    }

    private void refreshContactsCache() {
        final ProgressDialog progressDialog = ProgressDialog.show(this, "Refreshing...", "Refreshing contacts cache...", true, false);

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<SMSContact> contacts = new ArrayList<>();

                Cursor contactCursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
                contactCursor.moveToFirst();
                for (int i = 0; i < contactCursor.getCount(); i++) {
                    if (contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                        String name = contactCursor.getString(contactCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        int id = contactCursor.getInt(contactCursor.getColumnIndex(ContactsContract.Contacts._ID));

                        SMSContact contact = new SMSContact(name, id);

                        Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{String.valueOf(id)}, null);
                        while (phoneCursor.moveToNext()) {
                            contact.getPhones().add(phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)));
                        }
                        contacts.add(contact);
                    }
                    contactCursor.moveToNext();
                }
                contactCursor.close();

                SharedPreferences.Editor editor = preferences.edit();
                editor.putString(getString(R.string.pref_contact_key), new Gson().toJson(contacts, new TypeToken<List<SMSContact>>() {
                }.getType()));
                editor.apply();

                progressDialog.dismiss();
            }
        }).start();
    }
}
